FROM oraclelinux:latest
RUN yum install -y httpd git gcc ca-certificates make wget openssl-devel openssl
WORKDIR /var/www/html
RUN git clone https://github.com/igameproject/Breakout.git ./
EXPOSE 443 8080

RUN wget -O haproxy.tar.gz "https://www.haproxy.org/download/1.8/src/haproxy-1.8.14.tar.gz" &&\
	mkdir -p /usr/src/haproxy &&\
	tar -xzf haproxy.tar.gz -C /usr/src/haproxy --strip-components=1 &&\
	rm -f haproxy.tar.gz &&\
	makeOpts='TARGET=linux2628 USE_OPENSSL=1 USE_PCRE=1 PCREDIR= USE_ZLIB=1' &&\
	make -C /usr/src/haproxy -j "$(nproc)" all $makeOpts &&\
	make -C /usr/src/haproxy install-bin $makeOpts &&\
	mkdir -p /usr/local/etc/haproxy &&\
	cp -R /usr/src/haproxy/examples/errorfiles /usr/local/etc/haproxy/errors &&\
	rm -rf /usr/src/haproxy
	
RUN mkdir -p /etc/crt &&\
	cd /etc/crt &&\
	openssl req -newkey rsa:2048 -nodes -keyout key.key -x509 -days 365 -out crt.crt \
	-subj "/C=MA/ST=RABAT/L=RABAT/O=Oracle/CN=localhost" &&\
	cat key.key crt.crt | tee pem.pem

RUN sed -i '0,/Listen 80/s//Listen 8081/' /etc/httpd/conf/httpd.conf

RUN echo -e " \
global \\n\
    maxconn 4096 \\n\
    daemon \\n\
defaults \\n\
    mode tcp \\n\
    log 127.0.0.1 local0 notice \\n\
    maxconn 2000 \\n\
    option tcplog \\n\
    option dontlognull \\n\
    timeout connect 20s \\n\
    timeout client 10m \\n\
    timeout server 10m \\n\
listen stats \\n\
  bind :8080 \\n\
  mode http \\n\
  stats enable \\n\
  stats hide-version \\n\
  stats realm Oracle\ Haproxy\ Statistics \\n\
  stats uri /haproxy_stats \\n\
  stats auth admin:admin \\n\
frontend httpid \\n\
    mode tcp \\n\
    bind *:443 ssl crt /etc/crt/pem.pem \\n\
    default_backend breakout \\n\
backend breakout \\n\
    mode tcp \\n\
    balance roundrobin \\n\
    option redispatch \\n\
    cookie SERVERID insert indirect nocache \\n\
    server st1 localhost:8081 \\n\
    option ssl-hello-chk \\n"\
	> /usr/local/etc/haproxy/haproxy.cfg

WORKDIR /etc
RUN echo -e "#!/bin/bash \\nhaproxy -f /usr/local/etc/haproxy/haproxy.cfg \\n/usr/sbin/httpd -D FOREGROUND" > entrypoint.sh
RUN chmod a+x entrypoint.sh

CMD ./entrypoint.sh
